﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Utils
{
    public static class Arguments
    {
        public static string ConnectionString = "cs";

        public static string ConfigFile = "cfg";

        public static string EntityCount = "count";

        public static string BulkSize = "bs";

        public static string[] All = new string[] { ConnectionString, ConfigFile, EntityCount, BulkSize };

        public static Dictionary<string, string> Parse(string[] args)
        {
            var dict = new Dictionary<string, string>();

            for (int i = 0; i < args.Length; i++)
            {
                if (args[i].StartsWith("-"))
                {
                    var argument = new string(args[i].Skip(1).ToArray());

                    if (!All.Contains(argument))
                        throw new Exception($"Unknown argument '{argument}'.");

                    if ((i + 1) >= args.Length)
                        throw new Exception($"Argument '{argument}' has no value.");

                    dict.Add(argument, args[++i]);
                }
            }

            return dict;
        }
    }
}
