﻿namespace Utils
{
    public static class Defaults
    {
        public static string ConnectionStringName = "TasksDatabase";

        public static string ConfigFileName = "config.json";

        public static int ConsumersCount = 5;

        public static int TasksBulkSize = 5;
    }
}
