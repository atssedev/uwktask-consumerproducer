﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace Utils
{
    public static class Config
    {
        public static string GetConnectionStringFromJson(string filePath, string connectionStringName)
        {
            var configDirectory = Path.GetDirectoryName(filePath);
            var configFileName = Path.GetFileName(filePath);

            var configuration = new ConfigurationBuilder()
                    .SetBasePath(configDirectory)
                    .AddJsonFile(configFileName, false)
                    .Build();

            var connectionString = configuration.GetConnectionString(connectionStringName);

            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentException($"Specified connection string ({connectionStringName}) is not found in {configFileName}.");

            return connectionString;
        }
    }
}
