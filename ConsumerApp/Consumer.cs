﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Repository;

namespace ConsumerApp
{
    public class Consumer
    {
        private readonly TaskRepository _repository;
        private readonly int _bulkSize;
        private readonly int _consumerId;

        private static readonly Semaphore _pool = new Semaphore(1, 1);

        public Consumer(TaskRepository repository, int tasksBulkSize, int consumerId)
        {
            _repository = repository;
            _bulkSize = tasksBulkSize;
            _consumerId = consumerId;

            var thread = new Thread(async () => await ConsumeAsync());
            thread.Start();
        }

        public async Task ConsumeAsync()
        {
            while (true)
            {
                _pool.WaitOne();

                var tasks = await _repository.GetByStatusAsync(Repository.TaskStatus.Pending, _bulkSize);
                await _repository.UpdateManyAsync(tasks.Select(t => t.Id), Repository.TaskStatus.InProgress, _consumerId);

                _pool.Release();

                foreach (var task in tasks)
                {
                    await ResolveTask(task);
                }
            }
        }

        private async Task ResolveTask(TaskModel task)
        {
            try
            {
                // resolve the task
                Console.WriteLine(task.Text);
                Thread.Sleep(300);
            }
            catch
            {
                await _repository.UpdateAsync(task.Id, Repository.TaskStatus.Error, _consumerId);
                return;
            }

            await _repository.UpdateAsync(task.Id, Repository.TaskStatus.Done, _consumerId);
        }
    }
}
