﻿using System;
using System.IO;
using System.Threading.Tasks;
using Repository;
using Utils;

namespace ConsumerApp
{
    class Program
    {
        static async Task Main(string[] args)
        {
            string connectionString;

            int consumersCount;
            int tasksBulkSize;

            try
            {
                var arguments = Arguments.Parse(args);

                var configFileName = arguments.TryGetValue(Arguments.ConfigFile, out var cfg) ? cfg : Defaults.ConfigFileName;
                var configDirectory = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.Parent.FullName;

                var connectionStringName = arguments.TryGetValue(Arguments.ConnectionString, out var cs) ? cs : Defaults.ConnectionStringName;
                connectionString = Config.GetConnectionStringFromJson(Path.Combine(configDirectory, configFileName), connectionStringName);

                consumersCount = arguments.TryGetValue(Arguments.EntityCount, out var count) ? int.Parse(count) : Defaults.ConsumersCount;
                tasksBulkSize = arguments.TryGetValue(Arguments.BulkSize, out var bs) ? int.Parse(bs) : Defaults.TasksBulkSize;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();

                return;
            }

            Console.WriteLine("Consumer is running");

            var repository = new TaskRepository(connectionString);
            await repository.CreateTableIfNotExistsAsync();

            for (int i = 0; i < consumersCount; i++)
            {
                var consumer = new Consumer(repository, tasksBulkSize, i);
            }

            Console.ReadKey();
        }
    }
}
