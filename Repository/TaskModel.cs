﻿using System;

namespace Repository
{
    public class TaskModel
    {
        public int Id { get; set; }

        public DateTime CreationTime { get; set; }

        public DateTime? ModificationTime { get; set; }

        public DateTime? StatusInProgressTime { get; set; }

        public string Text { get; set; }

        public TaskStatus Status { get; set; }

        public int? ConsumerID { get; set; }
    }

    public enum TaskStatus
    {
        Pending = 1,
        InProgress,
        Done,
        Error,
    }
}
