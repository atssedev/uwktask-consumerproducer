﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class TaskRepository
    {
        private readonly string _connectionString;

        public TaskRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task CreateTableIfNotExistsAsync()
        {
            using var connection = new SqlConnection(_connectionString);
            await connection.OpenAsync();

            var command = new SqlCommand("select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'Tasks'", connection);
            var reader = await command.ExecuteReaderAsync();

            var tableExists = reader.HasRows;
            await reader.CloseAsync();

            if (!tableExists)
            {
                var sqlExpression = @"create table Tasks (
	                    [Id] int identity(1,1) not null,
	                    [CreationTime] datetime2(7) not null,
	                    [ModificationTime] datetime2(7) null,
	                    [StatusInProgressTime] datetime2(7) null,
	                    [Text] nvarchar(max) not null,
	                    [Status] smallint not null,
	                    [ConsumerID] int null,
	                    primary key clustered ([Id] asc)
                    )";

                command.CommandText = sqlExpression;
                await command.ExecuteNonQueryAsync();
            }
        }

        public async Task<IEnumerable<TaskModel>> GetAllAsync()
        {
            var tasks = new List<TaskModel>();

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();

                var command = new SqlCommand("select * from Tasks", connection);
                var reader = await command.ExecuteReaderAsync();

                if (reader.HasRows)
                {
                    while (await reader.ReadAsync())
                    {
                        tasks.Add(ComposeTaskModel(reader));
                    }
                }

                await reader.CloseAsync();
            }

            return tasks;
        }

        public async Task<IEnumerable<TaskModel>> GetByStatusAsync(TaskStatus status, int limit = 0)
        {
            var tasks = new List<TaskModel>();

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();

                var sqlExpression = $"select {(limit > 0 ? "top " + limit : "")} * from Tasks where Status = @status";
                var command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(new SqlParameter("@status", (short)status));

                var reader = await command.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    while (await reader.ReadAsync())
                    {
                        tasks.Add(ComposeTaskModel(reader));
                    }
                }

                await reader.CloseAsync();
            }

            return tasks;
        }

        public async Task<Dictionary<int, IEnumerable<TaskModel>>> GetLatestTasks(
            IEnumerable<int> consumerIds, int limit = 0)
        {
            var dict = new Dictionary<int, IEnumerable<TaskModel>>();

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();

                foreach (var id in consumerIds.Distinct())
                {
                    var tasks = new List<TaskModel>();
                    var sqlExpression = $"select {(limit > 0 ? "top " + limit : "")} * from Tasks " +
                        $"where ConsumerID = @id order by ModificationTime desc";

                    var command = new SqlCommand(sqlExpression, connection);
                    command.Parameters.Add(new SqlParameter("@id", id));

                    var reader = await command.ExecuteReaderAsync();
                    if (reader.HasRows)
                    {
                        while (await reader.ReadAsync())
                        {
                            tasks.Add(ComposeTaskModel(reader));
                        }
                    }

                    dict.Add(id, tasks);

                    await reader.CloseAsync();
                }
            }

            return dict;
        }

        public async Task<bool> CreateAsync(string text, TaskStatus status)
        {
            var rowsInserted = 0;

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();

                var sqlExpression = "insert into Tasks (CreationTime, Text, Status) values (@creationTime, @text, @status)";
                var command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(new SqlParameter("@creationTime", DateTime.UtcNow));
                command.Parameters.Add(new SqlParameter("@text", text));
                command.Parameters.Add(new SqlParameter("@status", (short)status));

                rowsInserted = await command.ExecuteNonQueryAsync();
            }

            return rowsInserted > 0;
        }

        public async Task<bool> UpdateAsync(int id, TaskStatus status, int consumerId)
        {
            var rowsUpdated = 0;

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();

                var inProgress = status == TaskStatus.InProgress;

                var sqlExpression = "update Tasks set ModificationTime = @modificationTime, " +
                    $"{(inProgress ? "StatusInProgressTime = @statusInProgressTime, " : "")}" +
                    "Status = @status, ConsumerID = @consumerId where Id = @id";

                var command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(new SqlParameter("@id", id));
                command.Parameters.Add(new SqlParameter("@modificationTime", DateTime.UtcNow));
                command.Parameters.Add(new SqlParameter("@status", (short)status));
                command.Parameters.Add(new SqlParameter("@consumerId", consumerId));

                if (inProgress)
                    command.Parameters.Add(new SqlParameter("@statusInProgressTime", DateTime.UtcNow));

                rowsUpdated = await command.ExecuteNonQueryAsync();
            }

            return rowsUpdated > 0;
        }

        public async Task<bool> UpdateManyAsync(IEnumerable<int> ids, TaskStatus status, int consumerId)
        {
            if (!ids.Any())
                return false;

            var rowsUpdated = 0;

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();

                var inProgress = status == TaskStatus.InProgress;
                var idsString = string.Join(", ", ids.Distinct());

                var sqlExpression = "update Tasks set ModificationTime = @modificationTime, " +
                    $"{(inProgress ? "StatusInProgressTime = @statusInProgressTime, " : "")}" +
                    $"Status = @status, ConsumerID = @consumerId where Id in (${idsString})";

                var command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(new SqlParameter("@modificationTime", DateTime.UtcNow));
                command.Parameters.Add(new SqlParameter("@status", (short)status));
                command.Parameters.Add(new SqlParameter("@consumerId", consumerId));

                if (inProgress)
                    command.Parameters.Add(new SqlParameter("@statusInProgressTime", DateTime.UtcNow));

                rowsUpdated = await command.ExecuteNonQueryAsync();
            }

            return rowsUpdated > 0;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var rowsDeleted = 0;

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();

                var sqlExpression = "delete from Tasks where Id = @id";
                var command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(new SqlParameter("@id", id));
                rowsDeleted = await command.ExecuteNonQueryAsync();
            }

            return rowsDeleted > 0;
        }

        public async Task<ReportModel> GetTasksReport()
        {
            var report = new ReportModel();
            var tasks = new List<TaskModel>();

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();

                var command = new SqlCommand("select * from Tasks", connection);

                var reader = await command.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    while (await reader.ReadAsync())
                    {
                        tasks.Add(ComposeTaskModel(reader));
                    }
                }

                await reader.CloseAsync();
            }

            static bool TaskSuccessfullyExecuted(TaskModel task) => task.Status == TaskStatus.Done &&
                task.ModificationTime.HasValue && task.StatusInProgressTime.HasValue && 
                task.ModificationTime.Value > task.StatusInProgressTime.Value;

            report.TaskStatus = tasks.GroupBy(t => t.Status).ToDictionary(g => g.Key, g => g.Count());

            report.AvgProcessingTime = TimeSpan.FromMilliseconds(tasks.Where(t => TaskSuccessfullyExecuted(t))
                .Average(t => TimeSpan.FromTicks(t.ModificationTime.Value.Ticks).TotalMilliseconds - 
                    TimeSpan.FromTicks(t.StatusInProgressTime.Value.Ticks).TotalMilliseconds));

            var allExecutedTasks = tasks.Where(t => TaskSuccessfullyExecuted(t) || t.Status == TaskStatus.Error);
            var failedTasks = tasks.Where(t => t.Status == TaskStatus.Error);

            report.ErrorRate = (double)failedTasks.Count() / allExecutedTasks.Count() * 100;

            return report;
        }

        private TaskModel ComposeTaskModel(SqlDataReader reader)
        {
            return new TaskModel
            {
                Id = reader.GetInt32(0),
                CreationTime = reader.GetDateTime(1),
                ModificationTime = !reader.IsDBNull(2) ? (DateTime?)reader.GetDateTime(2) : null,
                StatusInProgressTime = !reader.IsDBNull(3) ? (DateTime?)reader.GetDateTime(3) : null,
                Text = reader.GetString(4),
                Status = (TaskStatus)reader.GetInt16(5),
                ConsumerID = !reader.IsDBNull(6) ? (int?)reader.GetInt32(6) : null,
            };
        }
    }
}
