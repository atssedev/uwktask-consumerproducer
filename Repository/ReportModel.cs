﻿using System;
using System.Collections.Generic;

namespace Repository
{
    public class ReportModel
    {
        public Dictionary<TaskStatus, int> TaskStatus { get; set; }

        public TimeSpan AvgProcessingTime { get; set; }

        public double ErrorRate { get; set; }
    }
}
