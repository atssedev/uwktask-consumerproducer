﻿using System.Threading.Tasks;
using Repository;

namespace ProducerApp
{
    public class Producer
    {
        private readonly TaskRepository _repository;

        public Producer(TaskRepository repository)
        {
            _repository = repository;
        }

        public async Task ProduceAsync(string task)
        {
            await _repository.CreateAsync(task, Repository.TaskStatus.Pending);
        }
    }
}
