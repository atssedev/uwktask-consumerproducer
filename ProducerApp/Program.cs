﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Repository;
using Utils;

namespace ProducerApp
{
    class Program
    {
        static async Task Main(string[] args)
        {
            string connectionString;

            try
            {
                var arguments = Arguments.Parse(args);

                var configFileName = arguments.TryGetValue(Arguments.ConfigFile, out var cfg) ? cfg : Defaults.ConfigFileName;
                var configDirectory = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.Parent.FullName;

                var connectionStringName = arguments.TryGetValue(Arguments.ConnectionString, out var cs) ? cs : Defaults.ConnectionStringName;
                connectionString = Config.GetConnectionStringFromJson(Path.Combine(configDirectory, configFileName), connectionStringName);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();

                return;
            }

            Console.WriteLine("Producer is running");

            var repository = new TaskRepository(connectionString);
            await repository.CreateTableIfNotExistsAsync();

            var producer = new Producer(repository);
            var taskCounter = 1;

            while (true)
            {
                await producer.ProduceAsync("Task " + taskCounter++);
                Console.WriteLine("Task produced");

                Thread.Sleep(300);
            }
        }
    }
}
